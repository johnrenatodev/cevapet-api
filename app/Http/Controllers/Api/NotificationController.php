<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\TokenHelper;

use App\Alimentacao;
use App\Banho;
use App\Consulta;
use App\Doctor;
use App\Lazer;
use App\Medicamento;
use App\Outro;
use App\Pet;
use App\Pulga;
use App\Saude_Bucal;
use App\Vacina;
use App\Vermifugos;
use App\Notification;
use App\User;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $validator = \Validator::make($request->all(), [
            'token'           => 'required'          
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $notifications = Notification::where('user_id', $user_id)
                                      ->where('read', 0)
                                      ->where('data', date("Y-m-d"))
                                      ->get();

        $notifications = $notifications->each(function ($notification, $key) {
                            $notification->data = date_create($notification->data);
                            $notification->data = date_format($notification->data,"d/m/Y");
                        });

        return response()->json($notifications, 200);
       
        /*$atividades = [];

        $Alimentacao = Alimentacao::where('alimentacao.user_id', $user_id)
                                  ->join('pets', 'pets.id', 'alimentacao.pet_id')
                                  ->select([
                                    "alimentacao.id",
                                    "alimentacao.alimentacao",
                                    "alimentacao.date as data",
                                    "pets.name as pet_name"
                                  ])
                                  ->get();

        foreach ($Alimentacao as $key => $value) 
        {

            $data = date('d/m/Y',strtotime($value->data));

            $atividades[$key] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->alimentacao,
                "tipo"   => "Alimentacao",
                "id"     => $value->id

            ];
        }


        $counter = count($atividades);

        $Banho       = Banho::where('banhos.user_id', $user_id)
                            ->join('pets', 'pets.id', 'banhos.pet_id')
                            ->select([
                                "banhos.id",
                                "banhos.tipo",
                                "banhos.nome",
                                "banhos.date as data",
                                "pets.name as pet_name"
                            ])
                            ->get();


        foreach ($Banho as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->tipo . " " . $value->nome,
                "tipo"   => "Banhos",
                "id"     => $value->id

            ];

        }

        $Notification   = Notification::all();

        foreach ($Notification as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->title,
                "tipo"   => "Notification",
                "id"     => $value->id

            ];

        }

        $Consulta    = Consulta::where('consultas.user_id', $user_id)
                            ->join('pets', 'pets.id', 'consultas.pet_id')
                            ->select([
                                "consultas.id",
                                "consultas.consulta",
                                "consultas.date as data",
                                "pets.name as pet_name"
                            ])
                            ->get();

        foreach ($Consulta as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "- Consulta - " . $value->consulta,
                "tipo"   => "Consultas",
                "id"     => $value->id

            ];

        }


        $Lazer       = Lazer::where('lazeres.user_id', $user_id)
                            ->join('pets', 'pets.id', 'lazeres.pet_id')
                            ->select([
                                "lazeres.id",
                                "lazeres.descrever_atividade",
                                "lazeres.date as data",
                                "pets.name as pet_name"
                            ])
                            ->get();

        foreach ($Lazer as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));

            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->descrever_atividade,
                "tipo"   => "Lazeres",
                "id"     => $value->id

            ];

        }

        $Medicamento = Medicamento::where('medicamentos.user_id', $user_id)->join('pets', 'pets.id', 'medicamentos.pet_id')
                            ->select([
                                "medicamentos.id",
                                "medicamentos.nome",
                                "medicamentos.date as data",
                                "pets.name as pet_name"
                            ])
                            ->get();

        foreach ($Medicamento as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->nome,
                "tipo"   => "Medicamentos",
                "id"     => $value->id
            ];

        } 

        $Outro       = Outro::where('outros.user_id', $user_id)->join('pets', 'pets.id', 'outros.pet_id')
                            ->select([
                                "outros.id",
                                "outros.oque",
                                "outros.date as data",
                                "pets.name as pet_name"
                            ])
                            ->get();

        foreach ($Outro as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->oque,
                "tipo"   => "Outros",
                "id"     => $value->id
            ];

        }         

        $Pulga       = Pulga::where('pulgas.user_id', $user_id)->join('pets', 'pets.id', 'pulgas.pet_id')
                            ->select([
                                "pulgas.id",
                                "pulgas.nome",
                                "pulgas.modo_de_uso",
                                "pulgas.date as data",
                                "pets.name as pet_name"
                            ])
                        ->get();

        foreach ($Pulga as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->nome ." ". $value->modo_de_uso,
                "tipo"   => "Pulgas",
                "id"     => $value->id
            ];

        }  


        $Saude_Bucal = Saude_Bucal::where('saude__bucals.user_id', $user_id)->join('pets', 'pets.id', 'saude__bucals.pet_id')
                            ->select([
                                "saude__bucals.id",
                                "saude__bucals.tratamento",
                                "saude__bucals.date as data",
                                "pets.name as pet_name"
                            ])
                        ->get();

        foreach ($Saude_Bucal as $key => $value) 
        {

            $counter = $counter + 1;


            $data = date('d/m/Y',strtotime($value->data));

            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->tratamento,
                "tipo"   => "Saude Bucal",
                "id"     => $value->id
            ];

        }         

        $Vacina      = Vacina::where('vacinas.user_id', $user_id)->join('pets', 'pets.id', 'vacinas.pet_id')
                            ->select([
                                "vacinas.id",
                                "vacinas.qual_vacina",
                                "vacinas.data as data",
                                "pets.name as pet_name"
                            ])
                        ->get();

        foreach ($Vacina as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->qual_vacina,
                "tipo"   => "Vacinas",
                "id"     => $value->id
            ];

        }  

        $Vermifugos  = Vermifugos::where('vermifugos.user_id', $user_id)->join('pets', 'pets.id', 'vermifugos.pet_id')
                            ->select([
                                "vermifugos.id",
                                "vermifugos.nome",
                                "vermifugos.date as data",
                                "pets.name as pet_name"
                            ])
                        ->get();

        foreach ($Vermifugos as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->nome,
                "tipo"   => "Vermifugos",
                "id"     => $value->id
            ];

        }*/  

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * 
         * @todo colocar o campo descrição dps
         *  
         */
        $validator = \Validator::make($request->all(), [
            'title'          => 'required',
            'descricao'      => 'nullable',
            'date'           => 'required'
        ]);
        
        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $register = Notification::create([
            'title'       => $request->title,
            'data'        => $request->date,
            'read'        => 0,
            'from_ceva'   => true,
            'description' => $request->descricao,
            'type'        => 'Ceva',
            'user_id'     => 0
        ]);

        $users =  User::all();

        $users->map(function($user) use ($request){
            Notification::create([
                'title'       => $request->title,
                'data'        => $request->date,
                'read'        => 0,
                'description' => $request->descricao,
                'from_ceva'   => false,
                "tipo"        => "Notification", 
                'user_id'     => $user->id
            ]); 
        });

        if($register){

            session()->flash('status', 'Notificação cadastrada com sucesso!');

            return redirect()->back();
        }
        else{

            session()->flash('status', 'Error!');

            return redirect()->back();  
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'token'          => 'required',
            'id'             => 'required'
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $notifications = Notification::where('id', $request->id)->first();

        if(!$notifications){

            return response()->json([
                "content" => "Erro, notificação não encontrada!"
            ], 500);

        }
        else{



            $update = Notification::where('id', $request->id)
                                    ->update(['read' => true]);


            return response()->json([

                "contet" => $notifications

            ], 200);

        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateAll(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'token'           => 'required'          
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $notifications   = Notification::where('user_id', $user_id)
                                         ->where('read', 0)
                                         ->update(['read' => 1]);
       
        return response()->json([
                "mensage" => 'Notificacoes lidas com sucesso'
            ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
