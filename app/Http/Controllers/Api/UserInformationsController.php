<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserInformation;
use App\Helpers\TokenHelper;
use App\User;
use App\Helpers\ImageHelper;


// tentar fazer todos os campos do usuer informations.

class UserInformationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $validator = \Validator::make($request->all(), [
    //         'token'         => 'required|bail',
    //         'nome_completo' => 'required',
    //         'cpf'           => 'required',
    //         'dt_nascimento' => 'required',
    //         'sexo'          => 'required',
    //         'celular'       => 'required',
    //     ]);
        
    //     if ($validator->fails()) 
    //     {
    //         return response()->json([
    //             "error" => 'error with inputs'
    //         ], 500);
    //     }

    //     $valid = new TokenHelper;

    //     $valid = $valid->validToken($request->token);   

    //     if(!$valid){
    //         return response()->json([
    //             "error" => "Você não esta autorizado!"
    //         ], 500);
    //     }
    //     else{

    //         $user_id = $valid;

    //     }



    //     $register = UserInformation::create([
    //                     'nome_completo' => $request->nome_completo,
    //                     'cpf'           => $request->cpf,
    //                     'dt_nascimento' => $request->dt_nascimento,
    //                     'sexo'          => $request->sexo,
    //                     'celular'       => $request->celular,
    //                     'user_id'       => $user_id
    //                 ]);

    //     // atualizar login e senha aqui

    //     if($register){


    //         return response()->json([
    //             "mensage" => 'Informações cadastradas com sucesso'
    //         ], 200);

    //     }
    //     else{

    //         return response()->json([
    //             "error" => 'Falha ao registrar medico veterinario'
    //         ], 500);
    //     }
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'      => 'required|bail',
        ]);        

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $get = UserInformation::where('user_id', $user_id)->first();

        if($get){

            return response()->json([
                "content" => $get
            ], 200);

        }
        else{


            return response()->json([
                "error" => 'Usuario não tem Informações'
            ], 500);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // usuario n tem foto
        $validator = \Validator::make($request->all(), [
            'token'         => 'required|bail',
            'nome_completo' => 'nullable', 
            'cpf'           => 'nullable',   
            'dt_nascimento' => 'nullable',
            'sexo'          => 'nullable',   
            'celular'       => 'nullable',
            'image'         => 'nullable',
        ]);
        
        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid)
        {

            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        
        }
        else
        {

            $user_id = $valid;

        }

        $get_user_informations = UserInformation::where('user_id', $user_id)->first();

        if(!$get_user_informations){

            return response()->json([
                'error' => 'Usuario não encontrado'
            ], 500);

        }


        // save image

        if($request->image != $get_user_informations->image){
            $imageHelper = new ImageHelper();
            $imageHelper = $imageHelper->saveImage($request->image);
        }
        else{
            $imageHelper = $get_user_informations->image;
        }


        $update = UserInformation::where('user_id', $user_id)
        ->update([
            'nome_completo' => $request->nome_completo, 
            'cpf'           => $request->cpf,   
            'dt_nascimento' => $request->dt_nascimento,
            'sexo'          => $request->sexo,   
            'celular'       => $request->celular,
            'image'         => $imageHelper
        ]);

        if($update){

            return response()->json([
                "mensage" => 'Informações dos usuarios cadastradas com sucesso'
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao atualizado Informações do usuario'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
