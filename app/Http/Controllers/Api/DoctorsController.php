<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Doctor;
use App\Helpers\TokenHelper;


class DoctorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // lista todos os medicos
        $validator = \Validator::make($request->all(), [
            'token'    => 'required|bail',
        ]);        

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $getall = Doctor::where('user_id', $user_id)->get();

        if($getall){

            return response()->json([
                "content" => $getall 
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha so selecionar todos os medico veterinario'
            ], 500);
        
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'token'    => 'required|bail',
            'nome'     => 'required',
            'endereco' => 'nullable',
            'cidade'   => 'nullable', 
            'uf'       => 'nullable',
            'fixo'     => 'nullable',
            'celular'  => 'nullable',
            'email'    => 'nullable'
        ]);
        
        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }


        $register = Doctor::create([
                        'nome'     => $request->nome,
                        'endereco' => $request->endereco,
                        'cidade'   => $request->cidade,
                        'uf'       => $request->uf,
                        'fixo'     => $request->fixo,
                        'celular'  => $request->celular,
                        'email'    => $request->email,
                        'user_id'  => $user_id
                    ]);

        if($register){


            return response()->json([
                "mensage" => 'Medico veterinario registrado com sucesso'
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao registrar medico veterinario'
            ], 500);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'token'      => 'required|bail',
            'doctor_id'  => 'required'
        ]);        

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $get = Doctor::where('id', $request->doctor_id)->first();

        if($get){

            return response()->json([
                "content" => $get
            ], 200);

        }
        else{


            return response()->json([
                "error" => 'Medico veterinario não encontrado'
            ], 500);

        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

         $validator = \Validator::make($request->all(), [
            'token'    => 'required|bail',
            'doctor_id'=> 'required',
            'nome'     => 'required',
            'endereco' => 'nullable',
            'cidade'   => 'nullable', 
            'uf'       => 'nullable',
            'fixo'     => 'nullable',
            'celular'  => 'nullable',
            'email'    => 'nullable'
        ]);
        
        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }


        $get_doctor = Doctor::where('id', $request->doctor_id)->first();

        if(!$get_doctor){

            return response()->json([
                'error' => 'Veterinário não encontrado'
            ], 500);

        }

        $update = Doctor::where('id', $request->doctor_id)
        ->update([
            'nome'          => $request->nome,
            'endereco'      => $request->endereco,
            'cidade'        => $request->cidade,
            'uf'            => $request->uf,
            'fixo'          => $request->fixo,
            'celular'       => $request->celular,
            'email'         => $request->email
        ]);

        if($update){

            return response()->json([
                "mensage" => 'Veterinário atualizado com sucesso'
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao atualizar veterinário'
            ], 500);
        }

        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'token'      => 'required|bail',
            'doctor_id'  => 'required'
        ]);        

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $get = Doctor::where('id', $request->doctor_id)->first();

        $get = $get->delete();

        if($get){

            return response()->json([
                "content" => "Doctor deletado"
            ], 200);

        }
        else{


            return response()->json([
                "error" => 'Doctor não encontrado'
            ], 500);

        }


    }
}
