<?php

namespace App\Http\Controllers\Api;

// Atualizando sistemas.

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Consulta;
use App\Notification;
use App\Doctor;
use App\Helpers\TokenHelper;
use App\Helpers\RecurrenceHelper;



class ConsultasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'token'           => 'required',
            'consulta'        => 'required',
            'veterinario_id'  => 'required',
            'comentarios'     => 'nullable',
            'obs'             => 'nullable',
            'date'            => 'required',
            'data_ate'        => 'nullable',
            'pet_id'          => 'required',
            'recorrencia'     => 'nullable' 
        ]);
      
        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $veterinario_nome = Doctor::find($request->veterinario_id)->nome;

        if(isset($request->data_ate)){

             $recurrence = new RecurrenceHelper;
             $frequence_rule = $recurrence->createFrequenceRule($request->recorrencia);
             $dates = $recurrence->createRecurrences($request->date, $request->data_ate, $frequence_rule);

             foreach ($dates as $date) {

                 $register = Consulta::create([
                        'consulta'        =>  $request->consulta,
                        'veterinario_id'  =>  $request->veterinario_id,
                        'comentarios'     =>  $request->comentarios,
                        'obs'             =>  $request->obs,
                        'date'            =>  $date->getStart(),
                        'user_id'         =>  $user_id,
                        'pet_id'          =>  $request->pet_id
                    ]);

                $notification = new Notification();
                $notification_fields = [
                    "pet_id" => $request->pet_id, 
                    "desc" => $request->consulta, 
                    "more_info" => "Veterinário: " . $veterinario_nome . "|Comentários: ". $request->comentarios,
                    "tipo" => "Consulta", 
                    "data" => $date->getStart(),
                    "user_id" => $user_id
                ]; 
                $notification->createNotification($notification_fields);  
                             
             }
         }
        else{
                    $register = Consulta::create([
                        'consulta'        =>  $request->consulta,
                        'veterinario_id'  =>  $request->veterinario_id,
                        'comentarios'     =>  $request->comentarios,
                        'obs'             =>  $request->obs,
                        'date'            =>  $request->date,
                        'user_id'         =>  $user_id,
                        'pet_id'          =>  $request->pet_id
                    ]);

                $notification = new Notification();
                $notification_fields = [
                    "pet_id" => $request->pet_id, 
                    "desc" => $request->consulta, 
                    "more_info" => "Veterinário: " . $veterinario_nome . "|Comentários: ". $request->comentarios,
                    "tipo" => "Consulta", 
                    "data" => $request->date,
                    "user_id" => $user_id
                ]; 
                $notification->createNotification($notification_fields);                     
        }

        

        if($register){

            return response()->json([
                "mensage" => 'Consulta registrado com sucesso'
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao registrar Consulta'
            ], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'         => 'required',  
            'id'            => 'required' 
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $get = Consulta::find($request->id);

        if(!$get){
            
            return response()->json([

                "Error" =>  "Atividade não encontrada!"

            ], 500);
        
        }else{

            return response()->json([

                "Content" => $get

            ], 200);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'token'           => 'required',
            'consulta'        => 'required',
            'veterinario_id'  => 'required',
            'comentarios'     => 'nullable',
            'obs'             => 'nullable',
            'date'            => 'required',
            'data_ate'        => 'nullable',
            'pet_id'          => 'required',
            'recorrencia'     => 'nullable',
            'id'              => 'required' 
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $update = Consulta::where('id', $request->id)
        ->update([
            'consulta'        =>  $request->consulta,
            'veterinario_id'  =>  $request->veterinario_id,
            'comentarios'     =>  $request->comentarios,
            'obs'             =>  $request->obs,
            'date'            =>  $request->date,
            'user_id'         =>  $user_id,
            'pet_id'          =>  $request->pet_id
        ]);

        if($update){

            return response()->json([
                "mensage" => 'Atividade atualizada'
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao atualizar Atividade'
            ], 500);
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'      => 'required|bail',
            'id'         => 'required'
        ]);        

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $delete = Consulta::find($request->id)->delete();


        if($delete){

            return response()->json([
                "content" => "Deletado com sucesso"
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao deletar'
            ], 500);

        }
    }
}
