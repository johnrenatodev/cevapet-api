<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pet;
use App\Helpers\TokenHelper;
use App\Helpers\ImageHelper;
// aplicar o carbon aqui futuramente.

class PetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         // lista todos os medicos
        $validator = \Validator::make($request->all(), [
            'token'    => 'required|bail',
        ]);        

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $getall = Pet::where('user_id', $user_id)->get();

        if($getall){

            return response()->json([
                "content" => $getall 
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao selecionar todos os pets'
            ], 500);
        
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = \Validator::make($request->all(), [
            'token'         => 'required|bail',
            'name'          => 'required',
            'tipo'          => 'required', 
            'raca'          => 'nullable',
            'sexo'          => 'nullable',
            'dt_nascimento' => 'nullable',
            'peso'          => 'nullable',
            'image'         => 'nullable'
        ]);
        // pets somente nome e tipo

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        // save image
        $imageHelper = new ImageHelper();
        $imageHelper = $imageHelper->saveImage($request->image);

        $register = Pet::create([
            'name'          => $request->name,
            'tipo'          => $request->tipo,
            'raca'          => $request->raca,
            'sexo'          => $request->sexo,
            'dt_nascimento' => $request->dt_nascimento,
            'peso'          => $request->peso,
            'user_id'       => $user_id,
            'image'         => $imageHelper  
           // 'image'         => $request->image
        ]);

        if($register){


            return response()->json([
                "mensage" => 'Pet registrado com sucesso'
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao registrar pet'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'      => 'required|bail',
            'pet_id'     => 'required'
        ]);        

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $get = Pet::where('id', $request->pet_id)->where('user_id', $user_id)->first(); 

        if($get){

            return response()->json([
                "content" => $get
            ], 200);

        }
        else{


            return response()->json([
                "error" => 'Pet não encontrado'
            ], 500);

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'         => 'required|bail',
            'pet_id'        => 'required',
            'name'          => 'required',
            'tipo'          => 'required',
            'raca'          => 'nullable',
            'sexo'          => 'nullable',
            'dt_nascimento' => 'nullable',
            'peso'          => 'nullable',
            'image'         => 'nullable'
        ]);
        
        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $get_pet = Pet::where('id', $request->pet_id)->first();

        if(!$get_pet){

            return response()->json([
                'error' => 'Pet não encontrado'
            ], 500);

        }

        $update = Pet::where('id', $request->pet_id)
        ->update([
            'name'          => $request->name,
            'tipo'          => $request->tipo,
            'raca'          => $request->raca,
            'sexo'          => $request->sexo,
            'dt_nascimento' => $request->dt_nascimento,
            'peso'          => $request->peso,
            'user_id'       => $user_id,
            'image'         => $request->image
        ]);

        if($update){

            return response()->json([
                "mensage" => 'Pet atualizado com sucesso'
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao atualizado pet'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'      => 'required|bail',
            'pet_id'     => 'required'
        ]);        

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $get = Pet::where('id', $request->pet_id)->first();

        $get = $get->delete();

        if($get){

            return response()->json([
                "content" => "pet deletado"
            ], 200);

        }
        else{


            return response()->json([
                "error" => 'Pet não encontrado'
            ], 500);

        }
    }


    public function getType(Request $request){

        $validator = \Validator::make($request->all(), [
            'token'      => 'required|bail',
            'pet_id'     => 'required'
        ]);        

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        } 
 
        $get = Pet::where('id', $request->pet_id)->select(['tipo'])->first();

        if(!$get){
            return response()->json([
                "content" => "Error! Pet not fund!"
            ], 500);
        }
        else{
            return response()->json([
                "pet_type" => $get 
            ], 200);
        }

    }

}
