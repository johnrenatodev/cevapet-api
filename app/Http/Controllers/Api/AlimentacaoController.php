<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Alimentacao;
use App\Notification;
use App\Helpers\TokenHelper;
use App\Helpers\RecurrenceHelper;
// use Carbon\Carbon;
// use Carbon\CarbonPeriod;
// atualizar algumas coisas aqui dps

class AlimentacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'         => 'required',
            'alimentacao'   => 'required',
            'marca'         => 'nullable',
            'complemento'   => 'nullable',
            'tamanho'       => 'nullable',
            'data'          => 'required',
            'pet_id'        => 'required',
            'data_ate'      => 'nullable',
            'recorrencia'   => 'nullable' 
        ]);
       


        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        if(isset($request->data_ate)){

             $recurrence = new RecurrenceHelper;
             $frequence_rule = $recurrence->createFrequenceRule($request->recorrencia);
             $dates = $recurrence->createRecurrences($request->data, $request->data_ate, $frequence_rule);

             foreach ($dates as $date) {
                 $register = Alimentacao::create([
                                 'alimentacao'   =>  $request->alimentacao,
                                 'marca'         =>  $request->marca,
                                 'complemento'   =>  $request->complemento,
                                 'tamanho'       =>  $request->tamanho,
                                 'date'          =>  $date->getStart(),
                                 'user_id'       =>  $user_id,
                                 'pet_id'        =>  $request->pet_id
                             ]);

                $notification = new Notification();
                $notification_fields = [
                    "pet_id" => $request->pet_id, 
                    "desc" => $request->alimentacao,
                    "more_info" => "Marca: " . $request->marca . "|Complemento:" . $request->complemento . "|Tamanho: ". $request->tamanho,  
                    "tipo" => "Alimentação", 
                    "data" => $date->getStart(),
                    "user_id" => $user_id
                ]; 
                $notification->createNotification($notification_fields);           
             }
         }
        else{
                $register = Alimentacao::create([
                       'alimentacao'   =>  $request->alimentacao,
                       'marca'         =>  $request->marca,
                       'complemento'   =>  $request->complemento,
                       'tamanho'       =>  $request->tamanho,
                       'date'          =>  $request->data,
                       'user_id'       =>  $user_id,
                       'pet_id'        =>  $request->pet_id
                ]);

                $notification = new Notification();
                $notification_fields = [
                    "pet_id" => $request->pet_id, 
                    "desc" => $request->alimentacao,
                    "more_info" => "Marca: " . $request->marca . "|Complemento:" . $request->complemento . "|Tamanho: ". $request->tamanho,  
                    "tipo" => "Alimentação", 
                    "data" => $request->data,
                    "user_id" => $user_id
                ]; 
                $notification->createNotification($notification_fields);       
        }

        // checa se recebeu data d e data ate e faz a parte de recorrencia
        // 
        // se ele quiser semanalmente eu preciso encontrar
        // alguma maneira de calcular os dias 
        // se for diario eu rodo um script para inserir em todos os dias no proximo ano
        // se for semanal eu pego o dia e crio um script para obter o dia em questao e pegar todos os proimos exemplo hj é quarta,
        // eu quero todas as quartas em um ano
        // se for mensal eu pego todos os dias x nos proximos 12 messes
        // 
        // 

        if($register){


            return response()->json([
                "mensage" => 'Alimentacao registrado com sucesso'
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao registrar Alimentacao'
            ], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'token'         => 'required',  
            'id'            => 'required' 
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $get = Alimentacao::find($request->id);

        if(!$get){
            
            return response()->json([

                "Error" =>  "Atividade não encontrada!"

            ], 500);
        
        }else{

            return response()->json([

                "Content" => $get

            ], 200);

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'token'         => 'required',
            'alimentacao'   => 'required',
            'marca'         => 'nullable',
            'complemento'   => 'nullable',
            'tamanho'       => 'nullable',
            'data'          => 'required',
            'pet_id'        => 'required',
            'data_ate'      => 'nullable',
            'recorrencia'   => 'nullable',
            'id'            => 'nullable' 
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $update = Alimentacao::where('id', $request->id)
        ->update([
           'alimentacao'   =>  $request->alimentacao,
           'marca'         =>  $request->marca,
           'complemento'   =>  $request->complemento,
           'tamanho'       =>  $request->tamanho,
           'date'          =>  $request->data,
           'pet_id'        =>  $request->pet_id
        ]);

        if($update){

            return response()->json([
                "mensage" => 'Atividade atualizada'
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao atualizar Atividade'
            ], 500);
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'      => 'required|bail',
            'id'         => 'required'
        ]);        

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $delete = Alimentacao::find($request->id)->delete();


        if($delete){

            return response()->json([
                "content" => "Deletado com sucesso"
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao deletar'
            ], 500);

        }
    }
}
