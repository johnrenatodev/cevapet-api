<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\TokenHelper;
use App\Mail\Contato;
use Illuminate\Support\Facades\Mail;


class ContatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

       
       $validator = \Validator::make($request->all(), [
            'token'           => 'required', 
            'assunto'         => 'required',
            'mensagem'        => 'required'                
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        // sac@ceva.com
        $email = Mail::to('sac@ceva.com.br')->send(new Contato($request->assunto, $request->mensagem));

        if(!$email){
            return response()->json([

              'content' => 'Success to send email'

            ], 200);        
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email'    => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6'
        ]);
        
        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $register = User::create([
                        'name'     => rand(), // ajeito o nome quando o usuario for em configurações e ajeitar por la.
                        'email'    => $request->email,
                        'password' => Hash::make($request->password),
                     ]);

        if($register){

            $token = new TokenHelper;
  
            $token = $token->createToken($register->id);   

            return response()->json([
                "token" => $token
            ]);

        }
        else{
            return response()->json('error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
