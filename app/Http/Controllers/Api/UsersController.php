<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Mail;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Helpers\TokenHelper;

use App\UserInformation;

use App\Alimentacao;
use App\Banho;
use App\Consulta;
use App\Doctor;
use App\Lazer;
use App\Medicamento;
use App\Outro;
use App\Pet;
use App\Pulga;
use App\Saude_Bucal;
use App\Vacina;
use App\Vermifugos;
use App\Notification;


// Controllers
use App\Http\Controllers\Api\AlimentacaoController;        

use App\Http\Controllers\Api\BanhoController;           

use App\Http\Controllers\Api\ConsultasController;           

use App\Http\Controllers\Api\MedicamentosController;           

use App\Http\Controllers\Api\OutrosController;           

use App\Http\Controllers\Api\PulgasController;           

use App\Http\Controllers\Api\SaudeController;           

use App\Http\Controllers\Api\VacinasController;           

use App\Http\Controllers\Api\VermifugosController;           



class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'         => 'required|bail'
        ]);
        
        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid)
        {

            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        
        }
        else
        {

            $user_id = $valid;

        }

        $get_user = User::where('id', $user_id)->first();

        if(!$get_user){
            return response()->json([
                "Error" => "Usuario não encontrado"
            ], 500);
        }
        else{

            return response()->json([
                "content" => $get_user
            ], 200);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token' => 'required'
        ]);
        
        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }


        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid)
        {

            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        
        }
        else
        {

            $user_id = $valid;

        }

        $get_user = User::find($user_id);

        if(!$get_user){

            return response()->json([
                'error' => 'User not fund!'
            ], 500);
        
        }
        else{

            return response()->json([
                'content' => $get_user
            ], 200);

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'email'    => 'required|string|max:255',
            'password' => 'required|string|min:6',
            'tipo'     => 'required',
            'nome_completo' => 'nullable',
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }


        // checa se o usuario já existe
        $check_user = User::where('email', $request->email)->first();

        $token = new TokenHelper;

        if($check_user){
            
            if($check_user->tipo == 1){

                $token = $token->createToken($check_user->id);
                return response()->json([
                     "token" => $token
                ]);

            } else if($request->tipo == 1){
                $token = $token->createToken($check_user->id);
                    return response()->json([
                         "token" => $token
                    ]);

            }

        }

        $register = User::create([
                        'name'     => rand(), // ajeito o nome quando o usuario for em configurações e ajeitar por la.
                        'email'    => $request->email,
                        'tipo'     => $request->tipo,
                        'password' => Hash::make($request->password),
                     ]);

        $user_informations = UserInformation::create([
            'user_id' => $register->id,
            'nome_completo' => $request->nome_completo
        ]);

        if($register){
  
            $token = $token->createToken($register->id);   

            return response()->json([
                "token" => $token
            ]);

        }
        else{
            return response()->json('error');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function login(Request $request){

        $validator = \Validator::make($request->all(), [
            'email'    => 'required',
            'password' => 'required'
        ]);
    
        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $credentials = $request->only('email', 'password');

        if (\Auth::attempt($credentials))
        {


            $user = \Auth::user();

            if($user->tipo == 1){

                return response()->json("Email ou senha incorreta", 400);

            }

            $token = new TokenHelper;

            $token = $token->getToken($user->id);

            return response()->json([

                'token' => $token
            
            ]);

        }    

    }

    public function forgotPassword(Request $request){

        $validator = \Validator::make($request->all(), [
            'email'    => 'required'
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $codigo_reset = rand(100000,999999);


        $update = User::where('email', $request->email)->where('tipo', 0)->update([
            'codigo_reset' => $codigo_reset
        ]);

        if($update){
            $text = "Seu código para redefinição de senha: " . $codigo_reset;

            Mail::raw( $text, function($message) use ($request){
                $message->subject("Cevapet - Redefinição de senha");
                $message->to($request->email);
            });
            return response()->json('Um e-mail com o codigo para redefinicao de senha foi enviado', 200);
        }
        else{ 
            return response()->json('Esse e-mail nao esta cadastrado', 400);
        }
        
    }


    public function resetPassword(Request $request){

        $validator = \Validator::make($request->all(), [
            'email'    => 'required',
            'password' => 'required',
            'codigo_reset' => "required"
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 400);
        }

        $codigo_reset = rand(1000,9999);


        $user = User::where('email', $request->email)->where('tipo', 0)->first();

        if($user){
            if($request->codigo_reset == $user->codigo_reset){
                $user->password = Hash::make($request->password);
                $user->codigo_reset = null;
                $user->save();
                $token = new TokenHelper;
                $token = $token->createToken($user->id);   
                return response()->json([
                    "token" => $token
                ]);
            }
            else{
                return response()->json('Codigo invalido', 400);
            }
        }
        else{
            return response()->json('Esse e-mail nao existe', 400);
        }
    }

    public function listAll(Request $request){
       
       $validator = \Validator::make($request->all(), [
            'token'           => 'required'          
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }
       
        $atividades = [];

        $Alimentacao = Alimentacao::where('alimentacao.user_id', $user_id)
                                  ->join('pets', 'pets.id', 'alimentacao.pet_id')
                                  ->select([
                                    "alimentacao.id",
                                    "alimentacao.pet_id",
                                    "alimentacao.alimentacao",
                                    "alimentacao.date as data",
                                    "pets.name as pet_name"
                                  ])
                                  ->get();

        foreach ($Alimentacao as $key => $value) 
        {

            $data = date('d/m/Y',strtotime($value->data));

            $atividades[$key] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->alimentacao,
                "tipo"   => "Alimentacao",
                "id"     => $value->id,
                "pet_id" => $value->pet_id

            ];
        }


        $counter = count($atividades);

        $Banho       = Banho::where('banhos.user_id', $user_id)
                            ->join('pets', 'pets.id', 'banhos.pet_id')
                            ->select([
                                "banhos.id",
                                "banhos.pet_id",
                                "banhos.tipo",
                                "banhos.nome",
                                "banhos.date as data",
                                "pets.name as pet_name"
                            ])
                            ->get();


        foreach ($Banho as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->tipo . " " . $value->nome,
                "tipo"   => "Banhos",
                "id"     => $value->id,
                "pet_id" => $value->pet_id

            ];

        }

        $Consulta    = Consulta::where('consultas.user_id', $user_id)
                            ->join('pets', 'pets.id', 'consultas.pet_id')
                            ->select([
                                "consultas.id",
                                "consultas.pet_id",
                                "consultas.consulta",
                                "consultas.date as data",
                                "pets.name as pet_name"
                            ])
                            ->get();

        foreach ($Consulta as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "- Consulta - " . $value->consulta,
                "tipo"   => "Consultas",
                "id"     => $value->id,
                "pet_id" => $value->pet_id

            ];

        }


        $Lazer       = Lazer::where('lazeres.user_id', $user_id)
                            ->join('pets', 'pets.id', 'lazeres.pet_id')
                            ->select([
                                "lazeres.id",
                                "lazeres.pet_id",
                                "lazeres.descrever_atividade",
                                "lazeres.date as data",
                                "pets.name as pet_name"
                            ])
                            ->get();

        foreach ($Lazer as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));

            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->descrever_atividade,
                "tipo"   => "Lazeres",
                "id"     => $value->id,
                "pet_id" => $value->pet_id

            ];

        }

        $Medicamento = Medicamento::where('medicamentos.user_id', $user_id)->join('pets', 'pets.id', 'medicamentos.pet_id')
                            ->select([
                                "medicamentos.id",
                                "medicamentos.pet_id",
                                "medicamentos.nome",
                                "medicamentos.date as data",
                                "pets.name as pet_name"
                            ])
                            ->get();

        foreach ($Medicamento as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->nome,
                "tipo"   => "Medicamentos",
                "id"     => $value->id,
                "pet_id" => $value->pet_id
            ];

        } 

        $Outro       = Outro::where('outros.user_id', $user_id)->join('pets', 'pets.id', 'outros.pet_id')
                            ->select([
                                "outros.id",
                                "outros.pet_id",
                                "outros.oque",
                                "outros.date as data",
                                "pets.name as pet_name"
                            ])
                            ->get();

        foreach ($Outro as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->oque,
                "tipo"   => "Outros",
                "id"     => $value->id,
                "pet_id" => $value->pet_id
            ];

        }         

        $Pulga       = Pulga::where('pulgas.user_id', $user_id)->join('pets', 'pets.id', 'pulgas.pet_id')
                            ->select([
                                "pulgas.id",
                                "pulgas.pet_id",
                                "pulgas.nome",
                                "pulgas.modo_de_uso",
                                "pulgas.date as data",
                                "pets.name as pet_name"
                            ])
                        ->get();

        foreach ($Pulga as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->nome ." ". $value->modo_de_uso,
                "tipo"   => "Pulgas",
                "id"     => $value->id,
                "pet_id" => $value->pet_id
            ];

        }  


        $Saude_Bucal = Saude_Bucal::where('saude__bucals.user_id', $user_id)->join('pets', 'pets.id', 'saude__bucals.pet_id')
                            ->select([
                                "saude__bucals.id",
                                "saude__bucals.pet_id",
                                "saude__bucals.tratamento",
                                "saude__bucals.date as data",
                                "pets.name as pet_name"
                            ])
                        ->get();

        foreach ($Saude_Bucal as $key => $value) 
        {

            $counter = $counter + 1;


            $data = date('d/m/Y',strtotime($value->data));

            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->tratamento,
                "tipo"   => "Saude Bucal",
                "id"     => $value->id,
                "pet_id" => $value->pet_id
            ];

        }         

        $Vacina      = Vacina::where('vacinas.user_id', $user_id)->join('pets', 'pets.id', 'vacinas.pet_id')
                            ->select([
                                "vacinas.id",
                                "vacinas.pet_id",
                                "vacinas.qual_vacina",
                                "vacinas.data as data",
                                "pets.name as pet_name"
                            ])
                        ->get();

        foreach ($Vacina as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->qual_vacina,
                "tipo"   => "Vacinas",
                "id"     => $value->id,
                "pet_id" => $value->pet_id
            ];

        }  

        $Vermifugos  = Vermifugos::where('vermifugos.user_id', $user_id)->join('pets', 'pets.id', 'vermifugos.pet_id')
                            ->select([
                                "vermifugos.id",
                                "vermifugos.pet_id",
                                "vermifugos.nome",
                                "vermifugos.date as data",
                                "pets.name as pet_name"
                            ])
                        ->get();

        foreach ($Vermifugos as $key => $value) 
        {

            $counter = $counter + 1;

            $data = date('d/m/Y',strtotime($value->data));


            $atividades[$counter] = [

                "data"   => $data,
                "titulo" => $value->pet_name . "-" . $value->nome,
                "tipo"   => "Vermifugos",
                "id"     => $value->id,
                "pet_id" => $value->pet_id
            ];

        }  

        $collection = collect($atividades);
        $sorted = $collection->sortBy('data');
       
        return response()->json($sorted->values()->all(), 200);

    }

    public function registerTokenFirebase(Request $request){

        $validator = \Validator::make($request->all(), [
            'token'           => 'required',
            'tokenFireBase'   => 'required'          
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }


        $addToken = User::find($user_id)->update([
            "tokenFireBase" => $request->tokenFireBase
        ]);

        if(!$addToken){

            return response()->json([
                "Error" => "Erro ao inserir token"
            ], 500);

        } else { 

            return response()->json([
                "Content" => true
            ], 200);

        }

    }

    public function enableNotifications(Request $request){

        $validator = \Validator::make($request->all(), [
            'token'                  => 'required',
            'enable_notifications'   => 'required'          
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }


        $updateNotificationOption = User::find($user_id)->update([
            "enable_notifications" => $request->enable_notifications
        ]);
        

        if(!$updateNotificationOption){

            return response()->json([
                "Error" => "Erro ao inserir token"
            ], 500);

        } else { 

            return response()->json([
                "Content" => true
            ], 200);

        }


    }

    public function getNotificatiosFromCeva(Request $request){

       $validator = \Validator::make($request->all(), [
            'token'           => 'required'          
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $notificatios = Notification::where('type', 'Ceva')->where('data', '=', date('Y-m-d'))->get();
        $sorted = $notificatios->sortBy('data');
        return response()->json($sorted->values()->all(), 200);

    }

    public function getNotificationsState(Request $request){

        $validator = \Validator::make($request->all(), [
            'token'           => 'required'          
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $notificatioState = User::where('id', $user_id)->select(['enable_notifications'])->first();

        if(!$notificatioState){

            return response()->json([
                'Error' => 'Usuario não encontrado'
            ], 500);

        } else { 

            return response()->json([
                'content' => $notificatioState
            ], 200);

        }

    }

}
