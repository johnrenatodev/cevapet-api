<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\TokenHelper;
use App\Saude_Bucal;
use App\Notification;
use App\Doctor;
use App\Helpers\RecurrenceHelper;

class SaudeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'             => 'required',
            'tratamento'        => 'required',  
            'veterianario_id'   => 'required', 
            'comentario'        => 'nullable',
            'obs'               => 'nullable',
            'date'              => 'required',
            'data_ate'          => 'nullable',
            'recorrencia'       => 'nullable',
            'pet_id'            => 'required'            
        ]);
              
        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $veterinario_nome = Doctor::find($request->veterianario_id)->nome;

        if(isset($request->data_ate)){

             $recurrence = new RecurrenceHelper;
             $frequence_rule = $recurrence->createFrequenceRule($request->recorrencia);
             $dates = $recurrence->createRecurrences($request->date, $request->data_ate, $frequence_rule);

             foreach ($dates as $date) {

                 $register = Saude_Bucal::create([
                    'tratamento'        =>  $request->tratamento,  
                    'veterianario_id'   =>  $request->veterianario_id, 
                    'comentario'        =>  $request->comentario,
                    'obs'               =>  $request->obs,
                    'date'              =>  $date->getStart(),
                    'user_id'           =>  $user_id,
                    'pet_id'          =>  $request->pet_id
                ]);

                $notification = new Notification();
                $notification_fields = [
                    "pet_id" => $request->pet_id, 
                    "desc" => $request->tratamento,
                    "more_info" => "Veterinário: " . $veterinario_nome . "|Comentários: ". $request->comentario,
                    "tipo" => "Saude Bucal", 
                    "data" => $date->getStart(),
                    "user_id" => $user_id
                ]; 
                $notification->createNotification($notification_fields); 
             }
         }
        else{

           $register = Saude_Bucal::create([
                'tratamento'        =>  $request->tratamento,  
                'veterianario_id'   =>  $request->veterianario_id, 
                'comentario'        =>  $request->comentario,
                'obs'               =>  $request->obs,
                'date'              =>  $request->date,
                'user_id'           =>  $user_id,
                'pet_id'          =>  $request->pet_id
            ]);

            $notification = new Notification();
            $notification_fields = [
                    "pet_id" => $request->pet_id, 
                    "desc" => $request->tratamento,
                    "more_info" => "Veterinário: " . $veterinario_nome . "|Comentários: ". $request->comentario,
                    "tipo" => "Saude Bucal", 
                    "data" => $request->date,
                    "user_id" => $user_id
            ]; 
            $notification->createNotification($notification_fields);
                
        }

        

        if($register){

            return response()->json([
                "mensage" => 'Saude Bucal registrado com sucesso'
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao registrar Saude Bucal'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'         => 'required',  
            'id'            => 'required' 
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $get = Saude_Bucal::find($request->id);

        if(!$get){
            
            return response()->json([

                "Error" =>  "Atividade não encontrada!"

            ], 500);
        
        }else{

            return response()->json([

                "Content" => $get

            ], 200);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'token'             => 'required',
            'tratamento'        => 'required',  
            'veterianario_id'   => 'required', 
            'comentario'        => 'nullable',
            'obs'               => 'nullable',
            'date'              => 'required',
            'data_ate'          => 'nullable',
            'recorrencia'       => 'nullable',
            'pet_id'            => 'required',
            'id'                => 'required' 
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $update = Saude_Bucal::where('id', $request->id)
        ->update([
            'tratamento'        =>  $request->tratamento,  
            'veterianario_id'   =>  $request->veterianario_id, 
            'comentario'        =>  $request->comentario,
            'obs'               =>  $request->obs,
            'date'              =>  $request->date,
            'user_id'           =>  $user_id,
            'pet_id'            =>  $request->pet_id
        ]);

        if($update){

            return response()->json([
                "mensage" => 'Atividade atualizada'
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao atualizar Atividade'
            ], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token'      => 'required|bail',
            'id'         => 'required'
        ]);        

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with inputs'
            ], 500);
        }

        $valid = new TokenHelper;

        $valid = $valid->validToken($request->token);   

        if(!$valid){
            return response()->json([
                "error" => "Você não esta autorizado!"
            ], 500);
        }
        else{

            $user_id = $valid;

        }

        $delete = Saude_Bucal::find($request->id)->delete();


        if($delete){

            return response()->json([
                "content" => "Deletado com sucesso"
            ], 200);

        }
        else{

            return response()->json([
                "error" => 'Falha ao deletar'
            ], 500);

        }
    }
}
