<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TokenFireBase;

class _TokenFireBaseController extends Controller
{
 
    public function saveToken(Request $request){

       $validator = \Validator::make($request->all(), [
            'token'           => 'required',           
        ]);

       // modo de uso para veterinario id em vacinas

        if ($validator->fails()) 
        {
            return response()->json([
                "error" => 'error with token'
            ], 500);
        }

        $register = TokenFireBase::create([
        	"token" => $request->token
        ]);


        if($register){
        	return response()->json("token registrado com sucesso", 200);
        }else{
        	return response()->json("falha ao registrar token", 500);
        }


    }

}
