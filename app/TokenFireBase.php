<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TokenFireBase extends Model
{
	protected $table    = "token_fire_bases";

    protected $fillable = array('token');
}
