<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saude_Bucal extends Model
{

	protected $table    = "saude__bucals";

    protected $fillable = array('tratamento', 'veterianario_id', 'comentario', 'obs', 'date', 'user_id', 'pet_id');

}







