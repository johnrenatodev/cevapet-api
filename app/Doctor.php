<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{

	protected $table    = "doctors";

    protected $fillable = array('nome', 'endereco', 'cidade', 'uf', 'fixo', 'celular', 'email', 'user_id', 'pet_id');

}









