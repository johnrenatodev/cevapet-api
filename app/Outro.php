<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outro extends Model
{
	protected $table    = "outros";

    protected $fillable = array('oque', 'complemento', 'comentario', 'obs', 'date', 'user_id', 'pet_id');
    
}
