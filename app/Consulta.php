<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consulta extends Model
{
	protected $table    = "consultas";

    protected $fillable = array('consulta', 'veterinario_id', 'comentarios', 'obs', 'date', 'user_id', 'pet_id');
}
