<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
	protected $table    = "notifications";

    protected $fillable = array('title', 'data', 'read', 'type', 'description', 'user_id', 'from_ceva', 'sent');

    public function createNotification($fields){

        $pet_name = Pet::where('id', $fields['pet_id'])->first()->name;

        $notification = Notification::create([
            'title'  => $pet_name . "-" . $fields['desc'] ,
            'data'   => $fields['data'],
            'type'   => $fields['tipo'],
            'read'   => false,
            'description' => $fields['more_info'],
            'user_id' => $fields['user_id']
        ]);

    }
}
