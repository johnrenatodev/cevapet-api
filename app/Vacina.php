<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacina extends Model
{
	protected $table    = "vacinas";

    protected $fillable = array('qual_vacina', 'complemento', 'comentario', 'modo_de_uso', 'data',  'user_id', 'pet_id');
}

