<?php

namespace App\Helpers;

use App\Session;

Class TokenHelper{


	public function createToken($user_id = null){

		if(!$user_id){

			return false;

		}

		$token = crypt(time().rand().'cevapet', '$2a$' . '08' . '$' . md5($user_id) . '$');

		$session = Session::create([
                        'user_id'     => $user_id, 
                        'user_token'  => $token,
                ]);

		return $token;

	}


	public function getToken($user_id){

		if(!$user_id){

			return false;

		}

		$session = Session::where('user_id', $user_id)->first();

		if(!$session){

			$token = $this->createToken($user_id);

			return $token;

		}
		else{

			return $session->user_token;			
		
		}




	}

	public function validToken($token){

		$session = Session::where('user_token', $token)->first();

		if(!$session){

			// tentativa fake
			return false;

		}
		else{

			return $session->user_id;			
		
		}


	}

}	