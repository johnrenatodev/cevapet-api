<?php

namespace App\Helpers;

use App\Session;

Class ImageHelper{

	public function saveImage($base64){
        
        //get the base-64 from data
        $base64_str = substr($base64, strpos($base64, ",")+1);

        $name = rand().time().'image.png';

        //decode base64 string
        $image = base64_decode($base64_str);
        \Storage::disk('local')->put($name, $image);
        $storagePath = \Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        // return $storagePath.$name; 
        $path = 'https://cevapetagenda.twinkledata.com/'.$name; 
        return  $path;
        // $imageHelper = str_replace('/var/www/html/api/public/', 'https://cevapetagenda.twinkledata.com/', $imageHelper);

	}

}	