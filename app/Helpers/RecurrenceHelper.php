<?php

namespace App\Helpers;

use App\Session;

Class RecurrenceHelper{

    private $rule_relation = ["DAILY" => "Diário", 
                              "WEEKLY" => "Semanal", 
                              "MONTHLY" => "Mensal", 
                              "YEARLY" => "Anual"
                            ];

    public function createFrequenceRule($frequence){

        foreach($this->rule_relation as $key => $freq){

            if($frequence == $freq){
                $frequence_rule = $key;
            }

        }
        return $frequence_rule;
    }


	public function createRecurrences($data, $data_ate, $recorrencia){

		$timezone  = 'America/Sao_Paulo';
        $startDate = new \DateTime($data, new \DateTimeZone($timezone));
        $endDate = new \DateTime($data_ate, new \DateTimeZone($timezone));
        $endDate->modify('+1 day');
        $endDate = $endDate->format('Y-m-d');
        $rule      = new \Recurr\Rule('FREQ='.$recorrencia.';UNTIL='.$endDate.';', $startDate);
        $transformer = new \Recurr\Transformer\ArrayTransformer();

        $transformerConfig = new \Recurr\Transformer\ArrayTransformerConfig();
        $transformerConfig->enableLastDayOfMonthFix();
        $transformer->setConfig($transformerConfig);

        $dates = $transformer->transform($rule);

        return $dates;
    }

}	        
        
       