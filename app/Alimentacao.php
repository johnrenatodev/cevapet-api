<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alimentacao extends Model
{

	protected $table    = "alimentacao";

    protected $fillable = array('alimentacao', 'marca', 'complemento', 'tamanho', 'date', 'user_id', 'pet_id');

}



