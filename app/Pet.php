<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    
	protected $table    = "pets";

    protected $fillable = array('name', 'tipo', 'raca', 'sexo', 'dt_nascimento', 'peso', 'user_id', 'image', 'pet_id');

}
