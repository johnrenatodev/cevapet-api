<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banho extends Model
{
	protected $table    = "banhos";

	// tipo =  banho/tosa etc...
	// nome =  nome_local
    protected $fillable = array('tipo', 'nome', 'endereco', 'obs', 'date', 'peso', 'user_id', 'pet_id');
}