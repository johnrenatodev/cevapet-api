<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vermifugos extends Model
{
	protected $table    = "vermifugos";

    protected $fillable = array('nome', 'marca', 'Dosagem', 'modo_de_uso', 'date', 'user_id', 'pet_id');
}
