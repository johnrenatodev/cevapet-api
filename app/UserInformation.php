<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
	protected $table    = "user_informations";

    protected $fillable = array('nome_completo', 'cpf', 'dt_nascimento', 'sexo', 'celular', 'image', 'user_id');
    
}


