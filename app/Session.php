<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{

	protected $table    = "sessions";

    protected $fillable = array('user_id', 'user_token');
    
}
