<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class Atividades extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:activities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checar atividades';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $notifications = \DB::table('notifications')
                            ->select([
                               'notifications.id',
                               'notifications.title',
                               'notifications.description',
			                   'notifications.sent',
			                   'notifications.type',
		  	                   'notifications.from_ceva',
                               'users.tokenFireBase',
                               'users.enable_notifications'
                            ])
                            ->join('users', 'notifications.user_id', '=', 'users.id')
                            //->orWhere('from_ceva', true)
                            ->whereRaw('Date(data) = CURDATE()')
                            ->get();

        \Log::info($notifications);

        $old = 0;

        foreach ($notifications as $key => $value){

           if($value->type == "Ceva"){
                     \Log::info('entrei no ceva validator');
                    if(!$value->sent){
                    //\DB::table('notifications')->where('id', $value->id)->update(["sent" => true]);
		    $id = $value->id;
                    $notificationBuilder = new PayloadNotificationBuilder($value->title);
                    $notificationBuilder->setBody($value->description);
                    $optionBuilder = new OptionsBuilder();
                    $optionBuilder->setTimeToLive(60*20);
                    $notification = $notificationBuilder->build();
                    $option = $optionBuilder->build();
             	    foreach(\DB::table('users')->select('tokenFireBase')->get() as $key => $value ){
                     // \Log::info('value'.$value);
                      $token = $value->tokenFireBase;
                      $downstreamResponse = FCM::sendTo($token, $option, $notification);
                    }
                    \DB::table('notifications')->where('id', $id)->update(["sent" => true]);
                   }
                   // $token = $value->tokenFireBase;
                   // $downstreamResponse = FCM::sendTo($token, $option, $notification);
	   }



            if($old == $value->id){
             continue;
            }

            \Log::info('OLD => '.$old);

            if((!$value->tokenFireBase) || (!$value->enable_notifications) ) {
                continue;
            }

	    if(!$value->sent){
                    \DB::table('notifications')->where('id', $value->id)->update(["sent" => true]);
	            $notificationBuilder = new PayloadNotificationBuilder($value->title);
           	    $notificationBuilder->setBody($value->description);
            	    $optionBuilder = new OptionsBuilder();
                    $optionBuilder->setTimeToLive(60*20);
                    $notification = $notificationBuilder->build();
                    $option = $optionBuilder->build();
                    $token = $value->tokenFireBase;
                    $downstreamResponse = FCM::sendTo($token, $option, $notification);
                    $old = $value->id;
                    \Log::info('enviou '.$value->id);
            }

        }

    }
}
