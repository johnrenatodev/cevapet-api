<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
	protected $table    = "medicamentos";

    protected $fillable = array('nome', 'marca', 'Dosagem', 'modo_de_uso', 'date', 'proxima_data', 'proxima_data', 'user_id', 'pet_id');
}