@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="/api/notificacao/register"> 
                      <div class="form-group">
                        <label for="exampleFormControlInput1">Titulo da notificação</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Insira o titulo da notificação">
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlInput1">Descrição</label>
                        <textarea class="form-control" rows="5" id="descricao" name="descricao"></textarea>
                      </div>                      
                      <div class="form-group">
                        <label for="exampleFormControlInput1">Data da notificação</label>
                        <input type="date" class="form-control"  id="date" name="date" placeholder="Insira o titulo da notificação">
                      </div> 
                      <button type="submit" class="btn btn-primary">Cadastrar</button>                     
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
