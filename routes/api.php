<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



// Register a user 
Route::post('/user/register', 'Api\UsersController@store');

Route::post('/user/login', 'Api\UsersController@login');

Route::post('/user/forgotpassword', 'Api\UsersController@forgotPassword');

Route::post('/user/resetpassword', 'Api\UsersController@resetPassword');

Route::post('/user/registerTokenFirebase', 'Api\UsersController@registerTokenFirebase');

Route::post('/user/enable/notifications', 'Api\UsersController@enableNotifications');

Route::post('/user/getNotificatios/ceva', 'Api\UsersController@getNotificatiosFromCeva');

Route::post('/user/getNotificationsState', 'Api\UsersController@getNotificationsState');

// Route::post('/user/getUser', 'Api\UsersController@index');

// Para edita e deletar atividade
// Route::post('/atividades/getInfos', 'Api\UsersController@getInfos');

// Medico veterinario
Route::post('/doctor/register', 'Api\DoctorsController@store');

Route::post('/doctor/list', 'Api\DoctorsController@index');

Route::post('/doctor/select', 'Api\DoctorsController@show');

Route::post('/doctor/update', 'Api\DoctorsController@update');

Route::post('/doctor/delete', 'Api\DoctorsController@destroy');

// Pet
Route::post('/pet/register', 'Api\PetsController@store');

Route::post('/pet/list', 'Api\PetsController@index');

Route::post('/pet/select', 'Api\PetsController@show');

Route::post('/pet/update', 'Api\PetsController@update');

Route::post('/pet/delete', 'Api\PetsController@destroy');

Route::post('/pet/getType', 'Api\PetsController@getType');

// User informations
Route::post('/user/informations/get', 'Api\UserInformationsController@show');

// Route::post('/user/informations/create', 'Api\UserInformationsController@store');

Route::post('/user/informations/update', 'Api\UserInformationsController@update');

// Alimentação
Route::post('/alimentacao/register', 'Api\AlimentacaoController@store');
Route::post('/alimentacao/read', 'Api\AlimentacaoController@show');
Route::post('/alimentacao/destroy', 'Api\AlimentacaoController@destroy');
Route::post('/alimentacao/update', 'Api\AlimentacaoController@update');


// Banho 
Route::post('/banho/register', 'Api\BanhoController@store');
Route::post('/banho/read', 'Api\BanhoController@show');
Route::post('/banho/destroy', 'Api\BanhoController@destroy');
Route::post('/banho/update', 'Api\BanhoController@update');


// Saude Bucal
Route::post('/saude/register', 'Api\SaudeController@store');
Route::post('/saude/read', 'Api\SaudeController@show');
Route::post('/saude/destroy', 'Api\SaudeController@destroy');
Route::post('/saude/update', 'Api\SaudeController@update');

// Lazer
Route::post('/lazer/register', 'Api\LazerController@store');
Route::post('/lazer/read', 'Api\LazerController@show');
Route::post('/lazer/destroy', 'Api\LazerController@destroy');
Route::post('/lazer/update', 'Api\LazerController@update');


// Consultas
Route::post('/consultas/register', 'Api\ConsultasController@store');
Route::post('/consultas/read', 'Api\ConsultasController@show');
Route::post('/consultas/destroy', 'Api\ConsultasController@destroy');
Route::post('/consultas/update', 'Api\ConsultasController@update');


// Medicamentos
Route::post('/medicamentos/register', 'Api\MedicamentosController@store');
Route::post('/medicamentos/read', 'Api\MedicamentosController@show');
Route::post('/medicamentos/destroy', 'Api\MedicamentosController@destroy');
Route::post('/medicamentos/update', 'Api\MedicamentosController@update');

// Vermifugos 
Route::post('/vermifugos/register', 'Api\VermifugosController@store');
Route::post('/vermifugos/read', 'Api\VermifugosController@show');
Route::post('/vermifugos/destroy', 'Api\VermifugosController@destroy');
Route::post('/vermifugos/update', 'Api\VermifugosController@update');

// Pulgas
Route::post('/pulgas/register', 'Api\PulgasController@store');
Route::post('/pulgas/read', 'Api\PulgasController@show');
Route::post('/pulgas/destroy', 'Api\PulgasController@destroy');
Route::post('/pulgas/update', 'Api\PulgasController@update');

// Outros
Route::post('/outros/register', 'Api\OutrosController@store');
Route::post('/outros/read', 'Api\OutrosController@show');
Route::post('/outros/destroy', 'Api\OutrosController@destroy');
Route::post('/outros/update', 'Api\OutrosController@update');

// Vacina
Route::post('/vacina/register', 'Api\VacinaController@store');
Route::post('/vacina/read', 'Api\VacinaController@show');
Route::post('/vacina/destroy', 'Api\VacinaController@destroy');
Route::post('/vacina/update', 'Api\VacinaController@update');

// Consultar todas as atividades
Route::post('/atividades/getAll', 'Api\UsersController@listAll');

// contato 
Route::post('/contato/send', 'Api\ContatoController@index');

// Notificação
Route::post('/notificacao/register', 'Api\NotificationController@store');

Route::post('/notificacao/read', 'Api\NotificationController@show');

Route::post('/notificacao/getAll', 'Api\NotificationController@index');

Route::post('/notificacao/updateAll', 'Api\NotificationController@updateAll');



// Registrar token
//Route::post('/tokenfirebase/register', '_TokenFireBaseController@saveToken');

