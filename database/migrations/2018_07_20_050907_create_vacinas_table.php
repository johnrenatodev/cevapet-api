<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacinas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qual_vacina');
            $table->string('complemento')->nullable();
            $table->string('comentario')->nullable();
            $table->string('modo_de_uso')->nullable();
            $table->date('data');
            $table->integer('user_id');
            $table->integer('pet_id');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacinas');
    }
}
