<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLazersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lazeres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descrever_atividade');
            $table->string('comentario')->nullable();
            $table->string('complemento')->nullable();            
            $table->string('obs')->nullable();
            $table->date('date');
            $table->integer('user_id');
            $table->integer('pet_id');            
            $table->timestamps();        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lazers');
    }
}
