<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaudeBucalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saude__bucals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tratamento');
            $table->integer('veterianario_id')->nullable();
            $table->string('comentario')->nullable();
            $table->string('obs')->nullable();
            $table->date('date');
            $table->integer('user_id');
            $table->integer('pet_id');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saude__bucals');
    }
}
