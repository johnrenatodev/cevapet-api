<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlimentacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alimentacao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alimentacao');
            $table->string('marca')->nullable();
            $table->string('complemento')->nullable();
            $table->string('tamanho')->nullable();
            $table->date('date');
            $table->integer('user_id'); 
            $table->integer('pet_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alimentacaos');
    }
}
