<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('marca')->nullable();
            $table->string('Dosagem')->nullable();            
            $table->string('modo_de_uso')->nullable();
            $table->date('date');
            $table->date('proxima_data')->nullable();
            $table->integer('user_id');
            $table->integer('pet_id');            
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicamentos');
    }
}
